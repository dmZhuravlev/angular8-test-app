export interface Item {
    results: object[],
    page: number,
    total_results: number,
    total_pages: number
}

// export interface Params {
//     page: number,
//     text: string
// }