import { Component, OnInit } from '@angular/core';
import { SearchService } from '../../services/search/search.service';
import { Router, ActivatedRoute } from "@angular/router";
import { Item } from './interfaces.model';

@Component({
  selector: 'app-search-result',
  providers: [SearchService],
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.less']
})
export class SearchResultComponent implements OnInit {
  public items: Item;

  constructor(private router: Router, private route: ActivatedRoute, private _searchService: SearchService) {
    this.route.params.subscribe((params) => params.id && this.getItemsList(params.id));
  }

  getItemsList(text) {
    const searchText = atob(text);

    this._searchService.getItems(searchText)
      .subscribe((data: Item) => {
        this.items = data;
      });
  }

  goToDetailsPage(id) {
    this.router.navigate(['/details-page', id]);
  }

  ngOnInit() {
  }

}
