import { Component, OnInit } from '@angular/core';
import { Item } from './interfaces.model';
import { Router } from "@angular/router";
import { MatCardModule, MatIconModule } from '@angular/material';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.less']
})
export class SearchComponent implements OnInit {
  public items: Item;
  public pages: object;
  public searchText;

  constructor(
    private router: Router,
    public card: MatCardModule,
    public icon: MatIconModule) {
  }

  goSearchResult() {
    const text = this.searchText && this.searchText.trim();

    if (!text) {
      return;
    }

    this.router.navigate(['/serch-result', btoa(text)]);
  }

  ngOnInit() {
  }

}
// https://material.angular.io/guide/getting-started
