import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { SearchService } from '../../services/search/search.service';
// import { MatDividerModule } from '@angular/material';

@Component({
  selector: 'app-details-page',
  providers: [SearchService],
  templateUrl: './details-page.component.html',
  styleUrls: ['./details-page.component.less']
})

export class DetailsPageComponent implements OnInit {
  public movieInfo: object;

  constructor(private router: Router, private route: ActivatedRoute, private _searchService: SearchService) {
    this.route.params.subscribe((params) => params.id && this.getMoovieInfo(params.id));
  }

  ngOnInit() {
  }

  getMoovieInfo(id) {
    this._searchService.getMovieDetails(id)
      .subscribe((data) => {
        this.movieInfo = data;
      });
  }
}