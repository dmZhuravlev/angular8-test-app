import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './components/search/search.component';
import { DetailsPageComponent } from './components/details-page/details-page.component';
import { SearchResultComponent } from './components/search-result/search-result.component';
// import { AuthService } from './services/auth/auth.service';
// import { LoginComponent } from './components/login/login.component';

// Parent Routes
// https://www.tektutorialshub.com/angular/angular-child-routes-nested-routes/
const routes: Routes = [
  {
    path: '',
    component: SearchComponent,
    children: [
      {
        path: 'serch-result/:id',
        component: SearchResultComponent
      }
    ]
    // canActivate: [AuthService]
  },
  {
    path: 'details-page/:id',
    component: DetailsPageComponent,
  },
//   {
//     path: 'login',
//     component: LoginComponent
//   },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }