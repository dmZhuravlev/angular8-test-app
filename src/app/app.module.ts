import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
//import { enableProdMode } from '@angular/core';

// Modules
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { ToastrModule } from 'ngx-toastr';

// Services
// import { AuthService } from './services/auth/auth.service';
// import { UserService } from './services/user/user.service';
// import { SearchService } from './services/search/search.service';

// Pipes
// import { FilterPipe } from './pipes/filter.pipe';
// import { PhonePipe } from './pipes/phone.pipe';

// Components
// import { StudentListComponent } from './components/student/list/student-list.component';
// import { StudentDetailsComponent } from './components/student/details/student-details.component';
// import { StudentAddComponent } from './components/student/add/student-add.component';
// import { LoginComponent } from './components/login/login.component';
// import { HomeComponent, homeChildRoutes } from './components/home/home.component';
// import { HighlightStudentDirective } from './directives/highlight-student.directive';
import { AppRoutingModule } from './app-routing.module';
import { SearchComponent } from './components/search/search.component';
import { IndexComponent } from './components/index/index.component';
import { SearchResultComponent } from './components/search-result/search-result.component';
import { MatCardModule, MatButtonModule, MatIconModule } from '@angular/material';
import { DetailsPageComponent } from './components/details-page/details-page.component';

@NgModule({
	declarations: [
		SearchComponent,
		IndexComponent,
		DetailsPageComponent,
		SearchResultComponent,
		// StudentListComponent,
		// StudentDetailsComponent,
		// StudentAddComponent,
		// LoginComponent,
		// HomeComponent,
		// FilterPipe,
		// PhonePipe,
		// HighlightStudentDirective
	],
	imports: [
		BrowserModule,
		RouterModule,
		AppRoutingModule,
		FormsModule,
		ReactiveFormsModule,
		BrowserAnimationsModule,
		HttpClientModule,
		NoopAnimationsModule,
		MatCardModule,
		MatButtonModule,
		MatIconModule,
		// ToastrModule.forRoot({
		// 	timeOut: 3000,
		// 	positionClass: 'toast-bottom-right',
		// 	preventDuplicates: true,
		// }),
	],
  	providers: [],
	bootstrap: [IndexComponent]
})

// enableProdMode();

export class AppModule { }