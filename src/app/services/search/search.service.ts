import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import {Observable} from 'rxjs/Observable';
 
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const apiUrl = 'https://api.themoviedb.org/3/search/movie?api_key=40f28f2bdd6c3d98e12196cde3f1aa4c&query=';

@Injectable()
export class SearchService {
 
    constructor(private http: HttpClient) {}
 
    private getPageDetailsUrl(id) {
        return `https://api.themoviedb.org/3/movie/${id}?api_key=40f28f2bdd6c3d98e12196cde3f1aa4c`
    }

    getItems(queryStr, pageNumber?) {
        const pageParam = pageNumber ? `&page=${ pageNumber }` : '';

        return this.http.get(`${ apiUrl }${ queryStr.replace(/\s/g, '+') }${ pageParam }`);
    }

    getMovieDetails(id) {
        const url = this.getPageDetailsUrl(id);

        return this.http.get(url);
    }
}